package com.cmu.jaylerr.bikesquad.utility.dialogmodels;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.cmu.jaylerr.bikesquad.R;
import com.cmu.jaylerr.bikesquad.splash.views.SplashActivity;
import com.cmu.jaylerr.bikesquad.utility.sharedstring.SharedFlag;

/**
 * Created by jaylerr on 16-Feb-17.
 */

public class DialogMessageUtility extends Dialog implements View.OnClickListener {

    public Activity activity;
    private Button yes, no;
    private String title, message, target;
    private TextView messageView;
    private TextView titleView;

    public DialogMessageUtility(Activity activity, String title, String message, String target) {
        super(activity);
        this.activity = activity;
        this.title = title;
        this.message = message;
        this.target = target;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog_message);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        titleView = (TextView) findViewById(R.id.txt_title);
        messageView = (TextView) findViewById(R.id.txt_message);
        titleView.setText(title);
        messageView.setText(message);
        yes = (Button) findViewById(R.id.btn_ok);
        yes.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        TargetTask();
    }

    private void TargetTask(){
        dismiss();
        if (target.equals(null)){
            dismiss();
        }else if (target.equals(SharedFlag._flag_finish_activity)){
            activity.finish();
        }else if (target.equals(SharedFlag._flag_do_nothing)){
            dismiss();
        }else if (target.equals(SharedFlag._flag_restart_app)){
            Intent intent = new Intent(activity, SplashActivity.class);
            activity.startActivity(intent);
            activity.finish();
        }
    }
}