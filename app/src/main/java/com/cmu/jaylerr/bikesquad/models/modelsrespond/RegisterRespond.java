package com.cmu.jaylerr.bikesquad.models.modelsrespond;

/**
 * Created by jaylerr on 04-May-17.
 */

public class RegisterRespond {
    Boolean result;
    String message;

    public RegisterRespond(Boolean result, String message) {
        this.result = result;
        this.message = message;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
