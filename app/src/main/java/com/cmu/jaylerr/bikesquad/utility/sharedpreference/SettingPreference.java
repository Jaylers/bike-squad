package com.cmu.jaylerr.bikesquad.utility.sharedpreference;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.cmu.jaylerr.bikesquad.utility.sharedstring.SharedColor;
import com.cmu.jaylerr.bikesquad.utility.sharedstring.SharedKey;

/**
 * Created by jaylerr on 08-May-17.
 */

public class SettingPreference {
    private Activity activity;
    private SharedPreferences setting;
    private SharedPreferences.Editor editor;

    public SettingPreference(Context context){
        this.activity = (Activity) context;
        setting = activity.getSharedPreferences(SharedKey._key_setting, Context.MODE_PRIVATE);
        editor = setting.edit();
    }

    public void setSettingTheme(String user_state){
        editor.putString(SharedKey._key_setting_theme, user_state);
        editor.apply();
    }

    public String getSettingTheme(){
        return setting.getString(SharedKey._key_setting_theme, SharedColor._color_deep_green);
    }
}
