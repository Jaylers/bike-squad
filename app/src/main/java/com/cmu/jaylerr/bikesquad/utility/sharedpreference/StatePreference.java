package com.cmu.jaylerr.bikesquad.utility.sharedpreference;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.cmu.jaylerr.bikesquad.utility.sharedstring.SharedKey;

/**
 * Created by jaylerr on 08-May-17.
 */

public class StatePreference {
    private Activity activity;
    private SharedPreferences state;
    private SharedPreferences.Editor editor;

    public StatePreference(Context context){
        this.activity = (Activity) context;
        state = activity.getSharedPreferences(SharedKey._key_state, Context.MODE_PRIVATE);
        editor = state.edit();
    }

    public void setUserState(Boolean user_state){
        editor.putBoolean(SharedKey._key_state_signed_in, user_state);
        editor.apply();
    }

    public void setUserInformation( String username, String password){
        editor.putString(SharedKey._key_state_username, username);
        editor.putString(SharedKey._key_state_password, password);
        editor.apply();
    }

    public Boolean getState(){
        return state.getBoolean(SharedKey._key_state_signed_in, false);
    }

    public String getUserName(){
        return state.getString(SharedKey._key_state_username, "");
    }

    public String getPassword(){
        return state.getString(SharedKey._key_state_password, "");
    }
}
